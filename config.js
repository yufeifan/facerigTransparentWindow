const Vue = require('vue/dist/vue')
const ELEMENT = require('element-ui')
const settings = require('electron-settings')
const ipcRenderer = require('electron').ipcRenderer


Vue.use(ELEMENT)
let vm = new Vue({
    el: '#main',
    data: {
        config: settings.get('config') || {
            colorOffset: 2,
            smoothValue: 0,
            smoothValue: 0,
            isLeft: settings.get('isLeft') || true,
            xOffset: settings.get('xOffset') || 0,
        },
        fps: settings.get('fps') || 30,
        cameras: [],
        camera: settings.get('camera') || 'FaceRig Virtual Camera'
    },
    watch: {
        config: {
            handler(val, oldVal) {
                ipcRenderer.send('configChange', this.config)
            },
            deep: true
        },
    },
    methods: {
        async getCameras() {
            let x = await navigator.mediaDevices.enumerateDevices()
            let deviceNames = x.filter(d => d.kind == 'videoinput').map(d => {
                return d.label
            })
            return deviceNames
        },
        onChange() {
            settings.set('config', this.config)
        },
        onCameraChange() {
            settings.set('camera', this.camera)
            ipcRenderer.send('reload')
        },
        onFpsChange() {
            settings.set('fps', this.fps)
            ipcRenderer.send('reload')
            console.log(this.fps)
        }
    },
    async mounted() {
        this.cameras = await this.getCameras()
    }
})